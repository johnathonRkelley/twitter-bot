import tweepy, time, sys, config

#Twitter Access Variables
CONSUMER_KEY = config.CONSUMER_KEY #keep the quotes, replace this with your consumer key
CONSUMER_SECRET = config.CONSUMER_SECRET #keep the quotes, replace this with your consumer secret key
ACCESS_KEY = config.ACCESS_KEY #keep the quotes, replace this with your access token
ACCESS_SECRET = config.ACCESS_SECRET #keep the quotes, replace this with your access token secret

#Opens Authentication Handler
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

#Gets friends and followers
friends = api.friends_ids(screen_name="PCMotivatesYou")
followers = api.followers_ids(screen_name="PCMotivatesYou")

#Prints out Friends and Followers, not that it matters
for line in friends:
    print(line)

for line in followers:
    print(line)

#Makes an array for data to be compared and appended to.
remove = []

#Loops through friends to see if they are a follower
for friend in friends:
    notFriend = True    
    for follower in followers:
        if friend == follower:
            notFriend = False
            break

    if notFriend == True: #If they aren't following put in an array of id's
        remove.append(friend)

#Prints total followers and following
print("There are " , len(followers), " followers and following ", len(friends))

#Opens a file to write to
f = open("Unfollowed.txt", "a")

loop = 0 #Initiates an exit case
for unfollower in remove: #Loops through the remove array
    if loop == 75: #If 20 are unfollowed, breaks out of the loop
        break 
    else: #Gets rid of the unfollower
        line = "Unfollowing : " + str(unfollower) +  " : " +  api.get_user(id=unfollower).screen_name
        print(line)

        #Unfollows
        api.destroy_friendship(id=unfollower)

        time.sleep(2) #Waits 2 seconds and then unfollows again

        #Writes to file to declare who has been unfollowed
        f.write(line)
        f.write("\n")

        #Adds 1 to amount unfollowed
        loop = loop + 1

#Closes the file
f.close()

#Prints out ending stats
print("There are " , len(remove), " non-friends :(")
print("First Started Following :", len(friends))
print("Now Following :", len(api.friends_ids(screen_name="PCMotivatesYou")))

count = 0
for follower in followers:
    found = False
    for friend in friends:
        if friend == follower:
            found = True
            count = count + 1
            break

print("Followers that I am following back : " , count)