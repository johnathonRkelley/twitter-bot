import config
import tweepy

#Grabs Personal Tweets
page_list = []
n = 0
auth = tweepy.OAuthHandler(config.CONSUMER_KEY, config.CONSUMER_SECRET)
auth.set_access_token(config.ACCESS_KEY, config.ACCESS_SECRET)
api = tweepy.API(auth)


for page in tweepy.Cursor(api.user_timeline(screen_name = "Motivated Living"), count=200).pages(16):
    page_list.append(page)
    n = n+1
    print(n)

for page in page_list:
    for status in page:
       print (status.text)
