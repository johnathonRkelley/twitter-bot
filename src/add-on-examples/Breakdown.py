#   Created By : Johnathon Kelley
#   Description: Designed to remove characters from scraped data.
#   Line 8 - 16 : Removes basic input
#   Line 21 - 25: Removes HashTag
#
#

container = [] #Container for cleaned up tweets

file = open('AnotherGrabber_GrabbedTweets.txt', 'r') #File from AccountGrabber.py

#Basic Trimming
for line in file: #Goes through cuts off the end and beginning from getting the stdoutfrom AnotherGrabber
    container.append(line[2:-2])

file.close() #Closes

#Can use this to remove the ending of http
#or remove hashtags.
for line in container:
    print(line)
#remove hashtag
for line in container:
    index = line.find("#")
    if index is not -1:
        print(line[0:index])

#len(container) gets the size.
file = open('removed.txt', 'w')

container2 = []

ambiguous = {"xe2", "RT", "\\", "&amp", "xf0"}

count = 0
outOf = 0
for line in container:
    found = False
    for character in ambiguous:
        if character in line:
            found = True

    outOf += 1
    if found == False:
        #print(line)
        count += 1
        container2.append(line)
        #file.write(line)
        #file.write('\n')
#print("Final Count is : " , count, " out of " , outOf)
#file.close()



#file = open("somewhatCleaned.txt", "w")
cleaned = [] #Cleans http out of strings
for line in container:
    index = line.find("http")
    if index is not -1:
        print(line[0:index])
        cleaned.append(line[0:index])

container3 = []

for line in cleaned:
    index = line.find("\\n")
    if index is not -1:
        #print(line[0:index])
        #file.write(line[0:index])
        container3.append(line[0:index])
        #file.write("\n")
    else:
        #file.write(line)
        container3.append(line)
        #file.write("\n")

#file.close()

removedLines = []

file = open("remove.txt", "r")

for lines in file:
    removedLines.append(lines)

file.close()

for line in cleaned:
    found = False
    for remove in removedLines:
        index = line.find(remove)
        if index is not -1:
            found = True

    if found == False:
        print(line)

for line in cleaned:
    index = line.find("\\n")
    if index is -1:
        print(line[0:index])
    else:
        print(line)