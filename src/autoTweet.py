#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import tweepy, time, sys, config

#Twitter Access Variables
CONSUMER_KEY = config.CONSUMER_KEY
CONSUMER_SECRET = config.CONSUMER_SECRET
ACCESS_KEY = config.ACCESS_KEY
ACCESS_SECRET = config.ACCESS_SECRET

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

#Functions
def writeToFile(container):
    file = open("autoTweet.txt", "w")

    for line in container:
        line = line + '\n'
        file.write(line)

    file.close()


argfile = "autoTweet.txt"

filename=open(argfile,'r')
f=filename.readlines()
filename.close()

history = open("TweetHistory.txt", "a")

#Need to put into an array
tweets = []

#Puts into the array
for line in f:
    index = line.find("#quote")
    if index is not -1: #If #quote is found
        line = line.strip('\n')
        tweets.append(line)
    else: #If not found
        line = line.strip('\n')
        line = line + " #quote"
        tweets.append(line)

print("Beginning amount of Tweets : " , len(tweets))

while len(tweets) > 0:
    line = tweets[0]
    tweets.remove(line) #Deletes line after sent

    history.write(line)
    history.write('\n')

    writeToFile(tweets) #Helps reload file at the beginning

    print("Sending Following Tweet: ")
    print(line)

    api.update_status(line)
    try:
        time.sleep(5)
    except KeyboardInterrupt:
        history.close()


