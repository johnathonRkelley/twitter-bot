
# Twitter Bot

Twitter Bot is a script made in Python that is designed to automatically send tweets that are from a text file. As of the current design, it is suppose to send normal text tweets every thirty minutes. As the program sends the tweets, it will rewrite the "to send" tweets create a history file to store previous tweets. This would be a great program if you are looking to build up a quote twitter account. Check out mine at [PCMotivatesYou](https://twitter.com/PCMotivatesYou).


## Prerequisites

Make sure you have the prerequisites installed below. They are inside of the [requirements](https://bitbucket.org/johnathonRkelley/twitter-bot/src/master/requirements.txt) text file. 
 

Depending on the version of Python version you are using will depend on what pip you would use. pip2 (Python 2.7) or pip3 (Python 3).

```
pip2 install -r requirements.txt
pip3 install -r requirements.txt
```
## Utitlizing the script
The way the script was designed was to load tweets and then send tweets that came from a text file called "autoTweet.txt". The script will pull from this text file to distribute tweets. 

When looking at the imports, you will need to create a Python class called config. In this class you will have the following variables that you will need to include.

```
CONSUMER_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXX'  # keep the quotes, replace this with your consumer key
CONSUMER_SECRET = 'XXXXXXXXXXXXXXXXXXXXX'  # keep the quotes, replace this with your consumer secret key
ACCESS_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXXXX'  # keep the quotes, replace this with your access token
ACCESS_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXX'  # keep the quotes, replace this with your access token secret

```

You will need to register you app using the Twitter Developer portal. In this portal, you will the consumer key and secret. You will need to generate the other two keys. Once you get these keys, put them into a config file for the keys to be pulled from to work.

## Add On Examples
I am including some other examples as well for you to be able to pull from if you would like. This includes figuring out whether the people are following you and who you are following. 